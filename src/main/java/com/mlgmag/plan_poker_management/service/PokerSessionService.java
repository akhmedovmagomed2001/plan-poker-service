package com.mlgmag.plan_poker_management.service;

import com.mlgmag.plan_poker_management.model.PokerSession;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PokerSessionService {
    PokerSession save(PokerSession pokerSession);

    PokerSession update(PokerSession pokerSession);

    List<PokerSession> getAll();

    Optional<PokerSession> findByDisplayId(UUID displayId);

    void delete(PokerSession pokerSession);
}
