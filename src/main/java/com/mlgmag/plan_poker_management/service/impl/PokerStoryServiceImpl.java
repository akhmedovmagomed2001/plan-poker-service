package com.mlgmag.plan_poker_management.service.impl;

import com.mlgmag.plan_poker_management.model.PokerSession;
import com.mlgmag.plan_poker_management.model.PokerStory;
import com.mlgmag.plan_poker_management.model.PokerStoryStatus;
import com.mlgmag.plan_poker_management.repository.PokerStoryRepository;
import com.mlgmag.plan_poker_management.service.PokerSessionService;
import com.mlgmag.plan_poker_management.service.PokerStoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PokerStoryServiceImpl implements PokerStoryService {

    @Autowired
    private PokerStoryRepository pokerStoryRepository;
    @Autowired
    private PokerSessionService pokerSessionService;

    @Override
    public PokerStory save(PokerStory pokerStory) {
        UUID pokerSessionDisplayId = pokerStory.getPokerSession().getDisplayId();
        PokerSession pokerSession = pokerSessionService.findByDisplayId(pokerSessionDisplayId).orElseThrow();

        pokerStory.setPokerSession(pokerSession);

        pokerStory.setDisplayId(UUID.randomUUID());
        pokerStory.setStatus(PokerStoryStatus.PENDING);
        pokerStory.setEstimate(null);

        return pokerStoryRepository.save(pokerStory);
    }

    @Override
    public PokerStory update(PokerStory pokerStory) {
        return pokerStoryRepository.save(pokerStory);
    }

    @Override
    public List<PokerStory> getAll() {
        return pokerStoryRepository.findAll();
    }

    @Override
    public Optional<PokerStory> findByDisplayId(UUID displayId) {
        return Optional.ofNullable(pokerStoryRepository.findByDisplayId(displayId));
    }

    @Override
    public void delete(PokerStory pokerStory) {
        pokerStoryRepository.delete(pokerStory);
    }
}
