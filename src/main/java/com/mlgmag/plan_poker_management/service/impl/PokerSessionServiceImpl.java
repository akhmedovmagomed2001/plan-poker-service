package com.mlgmag.plan_poker_management.service.impl;

import com.mlgmag.plan_poker_management.model.PokerSession;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.repository.PokerSessionRepository;
import com.mlgmag.plan_poker_management.service.PokerSessionService;
import com.mlgmag.plan_poker_management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PokerSessionServiceImpl implements PokerSessionService {

    @Autowired
    private PokerSessionRepository pokerSessionRepository;
    @Autowired
    private UserService userService;

    @Override
    public PokerSession save(PokerSession pokerSession) {
        UUID createdByDisplayId = pokerSession.getCreatedBy().getDisplayId();
        User createdBy = userService.findByDisplayId(createdByDisplayId).orElseThrow();

        pokerSession.setCreatedBy(createdBy);

        Set<User> participants = Set.of(createdBy);
        pokerSession.setParticipants(participants);

        pokerSession.setDisplayId(UUID.randomUUID());
        return pokerSessionRepository.save(pokerSession);
    }

    @Override
    public PokerSession update(PokerSession pokerSession) {
        return pokerSessionRepository.save(pokerSession);
    }

    @Override
    public List<PokerSession> getAll() {
        return pokerSessionRepository.findAll();
    }

    @Override
    public Optional<PokerSession> findByDisplayId(UUID displayId) {
        return Optional.ofNullable(pokerSessionRepository.findByDisplayId(displayId));
    }

    @Override
    public void delete(PokerSession pokerSession) {
        pokerSession.setParticipants(new HashSet<>());
        pokerSessionRepository.delete(pokerSession);
    }
}
