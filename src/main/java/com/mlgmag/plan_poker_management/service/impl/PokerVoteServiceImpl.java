package com.mlgmag.plan_poker_management.service.impl;

import com.mlgmag.plan_poker_management.model.PokerStory;
import com.mlgmag.plan_poker_management.model.PokerVote;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.repository.PokerVoteRepository;
import com.mlgmag.plan_poker_management.service.PokerStoryService;
import com.mlgmag.plan_poker_management.service.PokerVoteService;
import com.mlgmag.plan_poker_management.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PokerVoteServiceImpl implements PokerVoteService {

    @Autowired
    private PokerVoteRepository pokerVoteRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private PokerStoryService pokerStoryService;

    @Override
    public PokerVote save(PokerVote pokerVote) {
        UUID votedByDisplayId = pokerVote.getVotedBy().getDisplayId();
        User votedBy = userService.findByDisplayId(votedByDisplayId).orElseThrow();
        pokerVote.setVotedBy(votedBy);

        UUID votedStoryDisplayId = pokerVote.getVotedStory().getDisplayId();
        PokerStory votedStory = pokerStoryService.findByDisplayId(votedStoryDisplayId).orElseThrow();
        pokerVote.setVotedStory(votedStory);

        pokerVote.setDisplayId(UUID.randomUUID());

        return pokerVoteRepository.save(pokerVote);
    }

    @Override
    public PokerVote update(PokerVote pokerVote) {
        return pokerVoteRepository.save(pokerVote);
    }

    @Override
    public List<PokerVote> getAll() {
        return pokerVoteRepository.findAll();
    }

    @Override
    public List<PokerVote> getAllByVotedStoryDisplayId(UUID votedStoryDisplayId) {
        return pokerVoteRepository.findByVotedStoryDisplayId(votedStoryDisplayId);
    }

    @Override
    public Optional<PokerVote> findByDisplayId(UUID displayId) {
        return Optional.ofNullable(pokerVoteRepository.findByDisplayId(displayId));
    }

    @Override
    public List<PokerVote> findByVotedStoryDisplayIdAndVotedByDisplayId(UUID votedStoryDisplayId, UUID votedByDisplayId) {
        return pokerVoteRepository.findByVotedStoryDisplayIdAndVotedByDisplayId(votedStoryDisplayId, votedByDisplayId);
    }
}
