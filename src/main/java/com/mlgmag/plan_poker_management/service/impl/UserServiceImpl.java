package com.mlgmag.plan_poker_management.service.impl;

import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.repository.UserRepository;
import com.mlgmag.plan_poker_management.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public User create(User user) {
        user.setDisplayId(UUID.randomUUID());

        String inputPassword = user.getPassword();
        String encodedPassword = passwordEncoder.encode(inputPassword);
        user.setPassword(encodedPassword);

        User registeredUser = userRepository.save(user);

        LOG.info("user '{}' successfully registered", registeredUser);

        return registeredUser;
    }

    @Override
    public List<User> getAll() {
        List<User> result = userRepository.findAll();
        LOG.info("{} users found", result.size());
        return result;
    }

    @Override
    public Optional<User> findByDisplayId(UUID displayId) {
        User user = userRepository.findByDisplayId(displayId);

        if (user == null) {
            LOG.warn("no user found by displayId '{}'", displayId);
        }

        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            LOG.warn("no user found by username '{}'", username);
        }

        return Optional.ofNullable(user);
    }

    @Override
    public Optional<User> findById(Long id) {
        Optional<User> user = userRepository.findById(id);

        if (user.isEmpty()) {
            LOG.warn("no user found by id '{}'", id);
        }

        return user;
    }
}
