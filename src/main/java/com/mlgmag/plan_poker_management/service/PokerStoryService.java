package com.mlgmag.plan_poker_management.service;

import com.mlgmag.plan_poker_management.model.PokerStory;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PokerStoryService {
    PokerStory save(PokerStory pokerStory);

    PokerStory update(PokerStory pokerStory);

    List<PokerStory> getAll();

    Optional<PokerStory> findByDisplayId(UUID displayId);

    void delete(PokerStory pokerStory);
}
