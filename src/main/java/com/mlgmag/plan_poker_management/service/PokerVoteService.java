package com.mlgmag.plan_poker_management.service;

import com.mlgmag.plan_poker_management.model.PokerVote;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PokerVoteService {
    PokerVote save(PokerVote pokerVote);

    PokerVote update(PokerVote pokerVote);

    List<PokerVote> getAll();

    List<PokerVote> getAllByVotedStoryDisplayId(UUID votedStoryDisplayId);

    Optional<PokerVote> findByDisplayId(UUID displayId);

    List<PokerVote> findByVotedStoryDisplayIdAndVotedByDisplayId(UUID votedStoryDisplayId, UUID votedByDisplayId);
}
