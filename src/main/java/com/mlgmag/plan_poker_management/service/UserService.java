package com.mlgmag.plan_poker_management.service;

import com.mlgmag.plan_poker_management.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

    User create(User user);

    List<User> getAll();

    Optional<User> findByDisplayId(UUID displayId);

    Optional<User> findByUsername(String username);

    Optional<User> findById(Long id);

}
