package com.mlgmag.plan_poker_management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlanPokerManagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlanPokerManagementApplication.class, args);
	}

}
