package com.mlgmag.plan_poker_management.jwt;

import com.mlgmag.plan_poker_management.model.User;

public final class JwtUserFactory {
    private JwtUserFactory() {
    }

    public static JwtUser create(User user) {
        return new JwtUser(user.getUsername(), user.getPassword());
    }
}
