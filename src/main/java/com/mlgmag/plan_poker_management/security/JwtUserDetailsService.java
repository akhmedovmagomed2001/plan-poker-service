package com.mlgmag.plan_poker_management.security;

import com.mlgmag.plan_poker_management.jwt.JwtUser;
import com.mlgmag.plan_poker_management.jwt.JwtUserFactory;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    private static final Logger LOG = LoggerFactory.getLogger(JwtUserDetailsService.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userService.findByUsername(username);

        if (userOptional.isEmpty()) {
            LOG.info("User with username '{}' not found", username);
            return null;
        }

        JwtUser jwtUser = JwtUserFactory.create(userOptional.get());
        LOG.info("User with username '{}' successfully loaded", username);
        return jwtUser;
    }
}
