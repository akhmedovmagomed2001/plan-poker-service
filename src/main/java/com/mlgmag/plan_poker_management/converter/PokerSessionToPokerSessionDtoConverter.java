package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.PokerSessionDto;
import com.mlgmag.plan_poker_management.model.PokerSession;
import com.mlgmag.plan_poker_management.model.User;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class PokerSessionToPokerSessionDtoConverter implements Converter<PokerSession, PokerSessionDto> {

    @Override
    public PokerSessionDto convert(PokerSession source) {
        PokerSessionDto destination = new PokerSessionDto();

        destination.setDisplayId(source.getDisplayId());
        destination.setTitle(source.getTitle());
        destination.setDescription(source.getDescription());
        destination.setCreatedByDisplayId(source.getCreatedBy().getDisplayId());

        Set<UUID> participantDisplayIds = source.getParticipants().stream().map(User::getDisplayId).collect(Collectors.toSet());
        destination.setParticipants(participantDisplayIds);

        return destination;
    }

}
