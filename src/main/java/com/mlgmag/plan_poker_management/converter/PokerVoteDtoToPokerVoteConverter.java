package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.PokerVoteDto;
import com.mlgmag.plan_poker_management.model.PokerStory;
import com.mlgmag.plan_poker_management.model.PokerVote;
import com.mlgmag.plan_poker_management.model.User;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class PokerVoteDtoToPokerVoteConverter implements Converter<PokerVoteDto, PokerVote> {

    @Override
    public PokerVote convert(PokerVoteDto source) {
        PokerVote destination = new PokerVote();

        destination.setValue(source.getValue());

        UUID votedByDisplayId = source.getVotedByDisplayId();
        User votedBy = new User();
        votedBy.setDisplayId(votedByDisplayId);
        destination.setVotedBy(votedBy);

        UUID votedStoryDisplayId = source.getVotedStoryDisplayId();
        PokerStory votedStory = new PokerStory();
        votedStory.setDisplayId(votedStoryDisplayId);
        destination.setVotedStory(votedStory);

        return destination;
    }
}
