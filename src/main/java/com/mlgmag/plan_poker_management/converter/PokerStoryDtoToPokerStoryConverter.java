package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.PokerStoryDto;
import com.mlgmag.plan_poker_management.model.PokerSession;
import com.mlgmag.plan_poker_management.model.PokerStory;
import org.springframework.stereotype.Component;

@Component
public class PokerStoryDtoToPokerStoryConverter implements Converter<PokerStoryDto, PokerStory> {

    @Override
    public PokerStory convert(PokerStoryDto source) {
        PokerStory destination = new PokerStory();
        destination.setTitle(source.getTitle());
        destination.setDescription(source.getDescription());
        destination.setEstimate(source.getEstimate());

        PokerSession pokerSession = new PokerSession();
        pokerSession.setDisplayId(source.getPokerSessionDisplayId());
        destination.setPokerSession(pokerSession);

        return destination;
    }

}
