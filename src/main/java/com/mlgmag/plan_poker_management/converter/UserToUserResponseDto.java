package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.UserResponseDto;
import com.mlgmag.plan_poker_management.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserToUserResponseDto implements Converter<User, UserResponseDto> {
    @Override
    public UserResponseDto convert(User source) {
        UserResponseDto destination = new UserResponseDto();
        destination.setUsername(source.getUsername());
        destination.setDisplayId(source.getDisplayId());
        destination.setFirstName(source.getFirstName());
        destination.setLastName(source.getLastName());
        return destination;
    }
}
