package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.PokerVoteDto;
import com.mlgmag.plan_poker_management.model.PokerVote;
import org.springframework.stereotype.Component;

@Component
public class PokerVoteToPokerVoteDtoConverter implements Converter<PokerVote, PokerVoteDto> {

    @Override
    public PokerVoteDto convert(PokerVote source) {
        PokerVoteDto destination = new PokerVoteDto();

        destination.setValue(source.getValue());
        destination.setDisplayId(source.getDisplayId());
        destination.setVotedByDisplayId(source.getVotedBy().getDisplayId());
        destination.setVotedStoryDisplayId(source.getVotedStory().getDisplayId());

        return destination;
    }
}
