package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.PokerStoryDto;
import com.mlgmag.plan_poker_management.model.PokerStory;
import org.springframework.stereotype.Component;

@Component
public class PokerStoryToPokerStoryDtoConverter implements Converter<PokerStory, PokerStoryDto> {

    @Override
    public PokerStoryDto convert(PokerStory source) {
        PokerStoryDto destination = new PokerStoryDto();

        destination.setDisplayId(source.getDisplayId());
        destination.setTitle(source.getTitle());
        destination.setDescription(source.getDescription());
        destination.setEstimate(source.getEstimate());
        destination.setPokerSessionDisplayId(source.getPokerSession().getDisplayId());
        destination.setStatus(source.getStatus());

        return destination;
    }

}
