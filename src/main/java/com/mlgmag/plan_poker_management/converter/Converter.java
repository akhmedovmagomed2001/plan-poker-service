package com.mlgmag.plan_poker_management.converter;

import java.util.Collection;
import java.util.List;

public interface Converter<S, D> {
    D convert(S source);

    default List<D> convertAll(Collection<S> sources) {
        return sources.stream().map(this::convert).toList();
    }
}
