package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.PokerSessionDto;
import com.mlgmag.plan_poker_management.model.PokerSession;
import com.mlgmag.plan_poker_management.model.User;
import org.springframework.stereotype.Component;

@Component
public class PokerSessionDtoToPokerSessionConverter implements Converter<PokerSessionDto, PokerSession> {

    @Override
    public PokerSession convert(PokerSessionDto source) {
        PokerSession destination = new PokerSession();

        destination.setTitle(source.getTitle());
        destination.setDescription(source.getDescription());

        User user = new User();
        user.setDisplayId(source.getCreatedByDisplayId());
        destination.setCreatedBy(user);

        return destination;
    }
}
