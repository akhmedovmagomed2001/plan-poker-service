package com.mlgmag.plan_poker_management.converter;

import com.mlgmag.plan_poker_management.dto.UserRequestDto;
import com.mlgmag.plan_poker_management.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserRequestDtoToUserConverter implements Converter<UserRequestDto, User> {

    @Override
    public User convert(UserRequestDto source) {
        User destination = new User();

        destination.setUsername(source.getUsername());
        destination.setPassword(source.getPassword());
        destination.setFirstName(source.getFirstName());
        destination.setLastName(source.getLastName());

        return destination;
    }

}
