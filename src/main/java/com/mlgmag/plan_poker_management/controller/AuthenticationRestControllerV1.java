package com.mlgmag.plan_poker_management.controller;

import com.mlgmag.plan_poker_management.converter.Converter;
import com.mlgmag.plan_poker_management.dto.AuthenticationRequestDto;
import com.mlgmag.plan_poker_management.dto.UserRequestDto;
import com.mlgmag.plan_poker_management.dto.UserResponseDto;
import com.mlgmag.plan_poker_management.jwt.JwtTokenProvider;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashMap;
import java.util.Map;

@Tag(name = "Auth V1")
@RestController
@RequestMapping(value = "/api/v1/auth")
public class AuthenticationRestControllerV1 {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private UserService userService;
    @Autowired
    private Converter<UserRequestDto, User> registrationRequestToUserConverter;
    @Autowired
    private Converter<User, UserResponseDto> userToRegistrationResponseDtoConverter;

    @PostMapping("/login")
    @Operation(summary = "Retrieve auth token")
    public ResponseEntity<Map<String, String>> login(@RequestBody AuthenticationRequestDto requestDto) {
        String username = requestDto.getUsername();
        String password = requestDto.getPassword();
        var authentication = new UsernamePasswordAuthenticationToken(username, password);
        authenticationManager.authenticate(authentication);

        if (userService.findByUsername(username).isEmpty()) {
            throw new UsernameNotFoundException("User with username '" + username + "' not found");
        }

        String token = jwtTokenProvider.createToken(username);

        Map<String, String> response = new HashMap<>();
        response.put("username", username);
        response.put("token", token);

        return ResponseEntity.ok(response);
    }

    @PostMapping("/register")
    @Operation(summary = "Register new user")
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponseDto register(@RequestBody UserRequestDto requestDto) {
        User user = registrationRequestToUserConverter.convert(requestDto);

        String username = user.getUsername();
        if (userService.findByUsername(username).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with username '" + username + "' already exists");
        }

        User registatedUser = userService.create(user);
        return userToRegistrationResponseDtoConverter.convert(registatedUser);
    }
}
