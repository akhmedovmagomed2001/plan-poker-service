package com.mlgmag.plan_poker_management.controller;

import com.mlgmag.plan_poker_management.converter.Converter;
import com.mlgmag.plan_poker_management.dto.PokerVoteDto;
import com.mlgmag.plan_poker_management.model.PokerStory;
import com.mlgmag.plan_poker_management.model.PokerStoryStatus;
import com.mlgmag.plan_poker_management.model.PokerVote;
import com.mlgmag.plan_poker_management.service.PokerStoryService;
import com.mlgmag.plan_poker_management.service.PokerVoteService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Tag(name = "Poker Vote V1")
@SecurityRequirement(name = "bearerAuthentication")
@RestController
@RequestMapping(value = "/api/v1/poker_vote")
public class PokerVoteControllerV1 {

    private static final Logger LOG = LoggerFactory.getLogger(PokerVoteControllerV1.class);

    @Autowired
    private PokerVoteService pokerVoteService;
    @Autowired
    private PokerStoryService pokerStoryService;
    @Autowired
    private Converter<PokerVoteDto, PokerVote> pokerVoteDtoToPokerVoteConverter;
    @Autowired
    private Converter<PokerVote, PokerVoteDto> pokerVoteToPokerVoteDtoConverter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PokerVoteDto create(@RequestBody PokerVoteDto requestDto) {
        PokerVote pokerVote = pokerVoteDtoToPokerVoteConverter.convert(requestDto);
        checkVoteStoryStatus(pokerVote);
        checkIfUserAlreadyVoted(pokerVote);
        PokerVote createdPokerVote = pokerVoteService.save(pokerVote);
        return pokerVoteToPokerVoteDtoConverter.convert(createdPokerVote);
    }

    @GetMapping
    public List<PokerVoteDto> get(
            @RequestParam(required = false) UUID displayId,
            @RequestParam(required = false) UUID votedStoryDisplayId
    ) {
        List<PokerVote> result = new ArrayList<>();

        if (displayId != null) {
            LOG.info("Retrieve vote with displayId '{}'", displayId);
            pokerVoteService.findByDisplayId(displayId).ifPresent(result::add);
        } else if (votedStoryDisplayId != null) {
            LOG.info("Retrieve all votes for story '{}'", votedStoryDisplayId);
            List<PokerVote> allByVotedStoryDisplayId = pokerVoteService.getAllByVotedStoryDisplayId(votedStoryDisplayId);
            result.addAll(allByVotedStoryDisplayId);
        }

        return pokerVoteToPokerVoteDtoConverter.convertAll(result);
    }

    @PatchMapping("/{displayId}")
    public PokerVoteDto update(
            @PathVariable(name = "displayId") UUID displayId,
            @RequestBody PokerVoteDto requestDto
    ) {
        PokerVote pokerVote = pokerVoteService.findByDisplayId(displayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Poker vote not found")
        );
        checkVoteStoryStatus(pokerVote);
        pokerVote.setValue(requestDto.getValue());
        PokerVote updatedPokerVote = pokerVoteService.update(pokerVote);
        return pokerVoteToPokerVoteDtoConverter.convert(updatedPokerVote);
    }

    private void checkVoteStoryStatus(PokerVote pokerVote) {
        UUID votedStoryDisplayId = pokerVote.getVotedStory().getDisplayId();
        PokerStory pokerStory = pokerStoryService.findByDisplayId(votedStoryDisplayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not find story for vote")
        );

        if (!PokerStoryStatus.VOTING.equals(pokerStory.getStatus())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not vote already voted or pending story");
        }
    }

    private void checkIfUserAlreadyVoted(PokerVote pokerVote) {
        UUID votedByDisplayId = pokerVote.getVotedBy().getDisplayId();
        UUID votedStoryDisplayId = pokerVote.getVotedStory().getDisplayId();

        boolean isUserAlreadyVoted = !pokerVoteService.findByVotedStoryDisplayIdAndVotedByDisplayId(
                votedStoryDisplayId,
                votedByDisplayId
        ).isEmpty();

        if (isUserAlreadyVoted) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User has already voted");
        }
    }

}
