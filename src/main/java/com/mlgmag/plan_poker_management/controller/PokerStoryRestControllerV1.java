package com.mlgmag.plan_poker_management.controller;

import com.mlgmag.plan_poker_management.converter.Converter;
import com.mlgmag.plan_poker_management.dto.PokerStoryDto;
import com.mlgmag.plan_poker_management.model.PokerStory;
import com.mlgmag.plan_poker_management.model.PokerStoryStatus;
import com.mlgmag.plan_poker_management.service.PokerStoryService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Tag(name = "Poker Story V1")
@SecurityRequirement(name = "bearerAuthentication")
@RestController
@RequestMapping(value = "/api/v1/poker_stories")
public class PokerStoryRestControllerV1 {

    private static final Logger LOG = LoggerFactory.getLogger(PokerStoryRestControllerV1.class);

    @Autowired
    private PokerStoryService pokerStoryService;
    @Autowired
    private Converter<PokerStoryDto, PokerStory> pokerStoryDtoToPokerStoryConverter;
    @Autowired
    private Converter<PokerStory, PokerStoryDto> pokerStoryToPokerStoryDtoConverter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PokerStoryDto create(@RequestBody PokerStoryDto requestDto) {
        PokerStory pokerStory = pokerStoryDtoToPokerStoryConverter.convert(requestDto);
        PokerStory createdPokerStory = pokerStoryService.save(pokerStory);
        return pokerStoryToPokerStoryDtoConverter.convert(createdPokerStory);
    }

    @GetMapping
    public List<PokerStoryDto> get(@RequestParam(required = false) UUID displayId) {
        List<PokerStory> result = new ArrayList<>();

        if (displayId != null) {
            LOG.info("Retrieve with displayId '{}'", displayId);
            pokerStoryService.findByDisplayId(displayId).ifPresent(result::add);
        } else {
            LOG.info("Retrieve all");
            result = pokerStoryService.getAll();
        }

        return pokerStoryToPokerStoryDtoConverter.convertAll(result);
    }

    @PatchMapping("/{displayId}")
    public PokerStoryDto update(
            @PathVariable(name = "displayId") UUID displayId,
            @RequestBody PokerStoryDto requestDto
    ) {
        PokerStory pokerStory = pokerStoryService.findByDisplayId(displayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Poker story not found")
        );

        checkStoryStatus(requestDto, pokerStory);

        if (requestDto.getTitle() != null) {
            pokerStory.setTitle(requestDto.getTitle());
        }

        if (requestDto.getDescription() != null) {
            pokerStory.setDescription(requestDto.getDescription());
        }

        if (requestDto.getStatus() != null) {
            pokerStory.setStatus(requestDto.getStatus());
        }

        if (requestDto.getEstimate() != null) {
            pokerStory.setEstimate(requestDto.getEstimate());
        }

        PokerStory updatedPokerStory = pokerStoryService.update(pokerStory);
        return pokerStoryToPokerStoryDtoConverter.convert(updatedPokerStory);
    }

    @DeleteMapping("/{displayId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(name = "displayId") UUID displayId) {
        PokerStory pokerStory = pokerStoryService.findByDisplayId(displayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Poker story not found")
        );

        pokerStoryService.delete(pokerStory);
    }

    private void checkStoryStatus(PokerStoryDto requestDto, PokerStory pokerStory) {
        if (PokerStoryStatus.PENDING.equals(requestDto.getStatus())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not change story status to `PENDING`");
        }

        if (PokerStoryStatus.VOTING.equals(requestDto.getStatus()) && PokerStoryStatus.VOTED.equals(pokerStory.getStatus())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not change status for the voted stories");
        }

        if (PokerStoryStatus.VOTED.equals(requestDto.getStatus()) && PokerStoryStatus.PENDING.equals(pokerStory.getStatus())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "It is impossible to skip the voting stage");
        }

        if (PokerStoryStatus.PENDING.equals(pokerStory.getStatus()) && requestDto.getEstimate() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "It is not possible to set estimate for pending story");
        }

        if (
                PokerStoryStatus.VOTED.equals(requestDto.getStatus()) &&
                        requestDto.getEstimate() == null &&
                        pokerStory.getEstimate() == null
        ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "It is impossible to skip estimate");
        }
    }
}
