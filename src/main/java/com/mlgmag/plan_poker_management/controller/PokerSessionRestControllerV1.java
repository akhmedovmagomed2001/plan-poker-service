package com.mlgmag.plan_poker_management.controller;

import com.mlgmag.plan_poker_management.converter.Converter;
import com.mlgmag.plan_poker_management.dto.PokerSessionDto;
import com.mlgmag.plan_poker_management.dto.PokerStoryDto;
import com.mlgmag.plan_poker_management.model.PokerSession;
import com.mlgmag.plan_poker_management.model.PokerStory;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.service.PokerSessionService;
import com.mlgmag.plan_poker_management.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Tag(name = "Poker Session V1")
@SecurityRequirement(name = "bearerAuthentication")
@RestController
@RequestMapping(value = "/api/v1/poker_sessions")
public class PokerSessionRestControllerV1 {

    private static final Logger LOG = LoggerFactory.getLogger(PokerSessionRestControllerV1.class);

    private static final String SESSION_NOT_FOUND_MESSAGE = "Poker session not found";

    @Autowired
    private PokerSessionService pokerSessionService;
    @Autowired
    private UserService userService;
    @Autowired
    private Converter<PokerSessionDto, PokerSession> pokerSessionDtoToPokerSessionConverter;
    @Autowired
    private Converter<PokerSession, PokerSessionDto> pokerSessionToPokerSessionDtoConverter;
    @Autowired
    private Converter<PokerStory, PokerStoryDto> pokerStoryToPokerStoryDtoConverter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PokerSessionDto create(@RequestBody PokerSessionDto requestDto) {
        PokerSession pokerSession = pokerSessionDtoToPokerSessionConverter.convert(requestDto);
        PokerSession createdPokerSession = pokerSessionService.save(pokerSession);
        return pokerSessionToPokerSessionDtoConverter.convert(createdPokerSession);
    }

    @GetMapping
    public List<PokerSessionDto> get(@RequestParam(required = false) UUID displayId) {
        List<PokerSession> result = new ArrayList<>();

        if (displayId != null) {
            LOG.info("Retrieve with displayId '{}'", displayId);
            pokerSessionService.findByDisplayId(displayId).ifPresent(result::add);
        } else {
            LOG.info("Retrieve all");
            result = pokerSessionService.getAll();
        }

        return pokerSessionToPokerSessionDtoConverter.convertAll(result);
    }

    @DeleteMapping("/{displayId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable(name = "displayId") UUID displayId) {
        PokerSession pokerSession = pokerSessionService.findByDisplayId(displayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, SESSION_NOT_FOUND_MESSAGE)
        );

        pokerSessionService.delete(pokerSession);
    }

    @PostMapping("/{displayId}/participant/{participantDisplayId}")
    @Operation(summary = "Add participant to poker session")
    public PokerSessionDto addParticipant(
            @PathVariable(name = "displayId") UUID displayId,
            @PathVariable(name = "participantDisplayId") UUID participantDisplayId
    ) {
        PokerSession pokerSession = pokerSessionService.findByDisplayId(displayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, SESSION_NOT_FOUND_MESSAGE)
        );
        User user = userService.findByDisplayId(participantDisplayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found")
        );
        pokerSession.getParticipants().add(user);

        PokerSession updatedPokerSession = pokerSessionService.update(pokerSession);
        return pokerSessionToPokerSessionDtoConverter.convert(updatedPokerSession);
    }

    @GetMapping("{displayId}/stories")
    @Operation(summary = "Get stories under the poker session")
    public List<PokerStoryDto> getStories(@PathVariable(name = "displayId") UUID displayId) {
        PokerSession pokerSession = pokerSessionService.findByDisplayId(displayId).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, SESSION_NOT_FOUND_MESSAGE)
        );

        Set<PokerStory> stories = pokerSession.getStories();

        return pokerStoryToPokerStoryDtoConverter.convertAll(stories);
    }
}
