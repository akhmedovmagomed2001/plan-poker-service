package com.mlgmag.plan_poker_management.controller;

import com.mlgmag.plan_poker_management.converter.Converter;
import com.mlgmag.plan_poker_management.dto.UserRequestDto;
import com.mlgmag.plan_poker_management.dto.UserResponseDto;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Tag(name = "Users V1")
@SecurityRequirement(name = "bearerAuthentication")
@RestController
@RequestMapping(value = "/api/v1/users")
public class UserRestControllerV1 {

    private static final Logger LOG = LoggerFactory.getLogger(UserRestControllerV1.class);

    @Autowired
    private UserService userService;
    @Autowired
    private Converter<UserRequestDto, User> userRequestDtoUserConverter;
    @Autowired
    private Converter<User, UserResponseDto> userToRegistrationResponseDtoConverter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponseDto create(@RequestBody UserRequestDto requestDto) {
        User user = userRequestDtoUserConverter.convert(requestDto);

        String username = user.getUsername();
        if (userService.findByUsername(username).isPresent()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User with username '" + username + "' already exists");
        }

        User registedUser = userService.create(user);
        return userToRegistrationResponseDtoConverter.convert(registedUser);
    }

    @GetMapping
    public List<UserResponseDto> get(
            @RequestParam(required = false) UUID displayId,
            @RequestParam(required = false) String username
    ) {
        List<User> result = new ArrayList<>();

        if (displayId != null) {
            LOG.info("Retrieve user by displayId '{}'", displayId);
            userService.findByDisplayId(displayId).ifPresent(result::add);
        } else if (username != null) {
            LOG.info("Retrieve user by username '{}'", username);
            userService.findByUsername(username).ifPresent(result::add);
        } else {
            LOG.info("Retrieve all users");
            result = userService.getAll();
        }

        return userToRegistrationResponseDtoConverter.convertAll(result);
    }

}
