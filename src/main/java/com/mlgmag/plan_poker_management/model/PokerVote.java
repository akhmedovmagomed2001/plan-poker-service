package com.mlgmag.plan_poker_management.model;

import jakarta.persistence.*;

import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "poker_votes")
public class PokerVote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "display_id", unique = true)
    private UUID displayId;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User votedBy;

    @ManyToOne
    @JoinColumn(name = "story_id", nullable = false)
    private PokerStory votedStory;

    @Column(name = "vote_value")
    private Integer value;

    public PokerVote() {
    }

    public PokerVote(Long id, UUID displayId, User votedBy, PokerStory votedStory, Integer value) {
        this.id = id;
        this.displayId = displayId;
        this.votedBy = votedBy;
        this.votedStory = votedStory;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public User getVotedBy() {
        return votedBy;
    }

    public void setVotedBy(User votedBy) {
        this.votedBy = votedBy;
    }

    public PokerStory getVotedStory() {
        return votedStory;
    }

    public void setVotedStory(PokerStory votedStory) {
        this.votedStory = votedStory;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PokerVote pokerVote = (PokerVote) o;
        return Objects.equals(id, pokerVote.id) && Objects.equals(displayId, pokerVote.displayId) && Objects.equals(votedBy, pokerVote.votedBy) && Objects.equals(votedStory, pokerVote.votedStory) && Objects.equals(value, pokerVote.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayId, votedBy, votedStory, value);
    }


}
