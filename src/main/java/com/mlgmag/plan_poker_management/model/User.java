package com.mlgmag.plan_poker_management.model;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "display_id", unique = true)
    private UUID displayId;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @ManyToMany(mappedBy = "participants")
    private Set<PokerSession> pokerSessions = new HashSet<>();

    @OneToMany(mappedBy = "createdBy")
    private Set<PokerSession> createdPokerSessions = new HashSet<>();

    @OneToMany(mappedBy = "votedBy")
    private Set<PokerVote> votes = new HashSet<>();

    public User() {
    }

    public User(Long id, UUID displayId, String username, String password, String firstName, String lastName, Set<PokerSession> pokerSessions, Set<PokerSession> createdPokerSessions, Set<PokerVote> votes) {
        this.id = id;
        this.displayId = displayId;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pokerSessions = pokerSessions;
        this.createdPokerSessions = createdPokerSessions;
        this.votes = votes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<PokerSession> getPokerSessions() {
        return pokerSessions;
    }

    public void setPokerSessions(Set<PokerSession> pokerSessions) {
        this.pokerSessions = pokerSessions;
    }

    public Set<PokerSession> getCreatedPokerSessions() {
        return createdPokerSessions;
    }

    public void setCreatedPokerSessions(Set<PokerSession> createdPokerSessions) {
        this.createdPokerSessions = createdPokerSessions;
    }

    public Set<PokerVote> getVotes() {
        return votes;
    }

    public void setVotes(Set<PokerVote> votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) && Objects.equals(displayId, user.displayId) && Objects.equals(username, user.username) && Objects.equals(password, user.password) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayId, username, password, firstName, lastName);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", displayId=" + displayId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
