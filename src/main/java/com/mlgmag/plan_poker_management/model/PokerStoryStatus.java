package com.mlgmag.plan_poker_management.model;

public enum PokerStoryStatus {
    PENDING, VOTING, VOTED
}
