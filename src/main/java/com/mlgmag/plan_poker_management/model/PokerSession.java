package com.mlgmag.plan_poker_management.model;

import jakarta.persistence.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "poker_sessions")
public class PokerSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "display_id", unique = true)
    private UUID displayId;

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false)
    private User createdBy;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "poker_session_user",
            joinColumns = {@JoinColumn(name = "poker_session_id")},
            inverseJoinColumns = {@JoinColumn(name = "user_id")}
    )
    private Set<User> participants = new HashSet<>();

    @OneToMany(mappedBy = "pokerSession", orphanRemoval = true)
    private Set<PokerStory> stories = new HashSet<>();

    public PokerSession() {
    }

    public PokerSession(Long id, UUID displayId, User createdBy, String title, String description, Set<User> participants, Set<PokerStory> stories) {
        this.id = id;
        this.displayId = displayId;
        this.createdBy = createdBy;
        this.title = title;
        this.description = description;
        this.participants = participants;
        this.stories = stories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public Set<PokerStory> getStories() {
        return stories;
    }

    public void setStories(Set<PokerStory> stories) {
        this.stories = stories;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PokerSession that = (PokerSession) o;
        return Objects.equals(id, that.id) && Objects.equals(displayId, that.displayId) && Objects.equals(createdBy, that.createdBy) && Objects.equals(title, that.title) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayId, createdBy, title, description);
    }

    @Override
    public String toString() {
        return "PokerSession{" +
                "id=" + id +
                ", displayId=" + displayId +
                ", createdBy=" + createdBy +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
