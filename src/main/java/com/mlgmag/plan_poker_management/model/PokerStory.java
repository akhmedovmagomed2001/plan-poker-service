package com.mlgmag.plan_poker_management.model;

import jakarta.persistence.*;

import java.util.*;

@Entity
@Table(name = "poker_story")
public class PokerStory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "display_id", unique = true)
    private UUID displayId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "estimate")
    private Integer estimate;

    @ManyToOne
    @JoinColumn(name = "session_id", nullable = false)
    private PokerSession pokerSession;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PokerStoryStatus status;

    @OneToMany(mappedBy = "votedStory")
    private Set<PokerVote> votes = new HashSet<>();

    public PokerStory() {
    }

    public PokerStory(Long id, UUID displayId, String title, String description, Integer estimate, PokerSession pokerSession, PokerStoryStatus status, Set<PokerVote> votes) {
        this.id = id;
        this.displayId = displayId;
        this.title = title;
        this.description = description;
        this.estimate = estimate;
        this.pokerSession = pokerSession;
        this.status = status;
        this.votes = votes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PokerSession getPokerSession() {
        return pokerSession;
    }

    public void setPokerSession(PokerSession pokerSession) {
        this.pokerSession = pokerSession;
    }

    public PokerStoryStatus getStatus() {
        return status;
    }

    public void setStatus(PokerStoryStatus status) {
        this.status = status;
    }

    public Set<PokerVote> getVotes() {
        return votes;
    }

    public void setVotes(Set<PokerVote> votes) {
        this.votes = votes;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PokerStory that = (PokerStory) o;
        return Objects.equals(id, that.id) && Objects.equals(displayId, that.displayId) && Objects.equals(title, that.title) && Objects.equals(description, that.description) && Objects.equals(estimate, that.estimate) && Objects.equals(pokerSession, that.pokerSession) && status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, displayId, title, description, estimate, pokerSession, status);
    }

    @Override
    public String toString() {
        return "PokerStory{" +
                "id=" + id +
                ", displayId=" + displayId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", estimate=" + estimate +
                ", pokerSession=" + pokerSession +
                ", status=" + status +
                ", votes=" + votes +
                '}';
    }
}
