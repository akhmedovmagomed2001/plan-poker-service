package com.mlgmag.plan_poker_management.repository;

import com.mlgmag.plan_poker_management.model.PokerSession;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PokerSessionRepository extends JpaRepository<PokerSession, Long> {
    PokerSession findByDisplayId(UUID displayId);
}
