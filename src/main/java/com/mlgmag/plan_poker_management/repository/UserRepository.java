package com.mlgmag.plan_poker_management.repository;

import com.mlgmag.plan_poker_management.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByDisplayId(UUID displayId);
}
