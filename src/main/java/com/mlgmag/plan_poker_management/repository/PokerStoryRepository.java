package com.mlgmag.plan_poker_management.repository;

import com.mlgmag.plan_poker_management.model.PokerStory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PokerStoryRepository extends JpaRepository<PokerStory, Long> {
    PokerStory findByDisplayId(UUID displayId);
}
