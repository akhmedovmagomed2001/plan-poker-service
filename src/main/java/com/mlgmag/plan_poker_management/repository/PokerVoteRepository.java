package com.mlgmag.plan_poker_management.repository;

import com.mlgmag.plan_poker_management.model.PokerVote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface PokerVoteRepository extends JpaRepository<PokerVote, Long> {
    PokerVote findByDisplayId(UUID displayId);

    @Query("SELECT v FROM PokerVote v JOIN v.votedStory s WHERE s.displayId = :votedStoryDisplayId")
    List<PokerVote> findByVotedStoryDisplayId(@Param("votedStoryDisplayId") UUID displayId);

    @Query("SELECT v FROM PokerVote v JOIN v.votedStory s Join v.votedBy vb WHERE s.displayId = :votedStoryDisplayId AND vb.displayId = :votedByDisplayId")
    List<PokerVote> findByVotedStoryDisplayIdAndVotedByDisplayId(
            @Param("votedStoryDisplayId") UUID votedStoryDisplayId,
            @Param("votedByDisplayId") UUID votedByDisplayId
    );
}
