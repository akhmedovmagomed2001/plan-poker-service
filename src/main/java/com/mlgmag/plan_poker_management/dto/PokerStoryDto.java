package com.mlgmag.plan_poker_management.dto;

import com.mlgmag.plan_poker_management.model.PokerStoryStatus;

import java.util.UUID;

public class PokerStoryDto {

    private UUID displayId;
    private String title;
    private String description;
    private Integer estimate;
    private UUID pokerSessionDisplayId;
    private PokerStoryStatus status;

    public PokerStoryDto() {
    }

    public PokerStoryDto(UUID displayId, String title, String description, Integer estimate, UUID pokerSessionDisplayId, PokerStoryStatus status) {
        this.displayId = displayId;
        this.title = title;
        this.description = description;
        this.estimate = estimate;
        this.pokerSessionDisplayId = pokerSessionDisplayId;
        this.status = status;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UUID getPokerSessionDisplayId() {
        return pokerSessionDisplayId;
    }

    public void setPokerSessionDisplayId(UUID pokerSessionDisplayId) {
        this.pokerSessionDisplayId = pokerSessionDisplayId;
    }

    public PokerStoryStatus getStatus() {
        return status;
    }

    public void setStatus(PokerStoryStatus status) {
        this.status = status;
    }

    public Integer getEstimate() {
        return estimate;
    }

    public void setEstimate(Integer estimate) {
        this.estimate = estimate;
    }
}
