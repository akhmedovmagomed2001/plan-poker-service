package com.mlgmag.plan_poker_management.dto;

import java.util.Objects;
import java.util.UUID;

public class UserResponseDto {

    private UUID displayId;
    private String username;
    private String firstName;
    private String lastName;

    public UserResponseDto() {
    }

    public UserResponseDto(UUID displayId, String username, String firstName, String lastName) {
        this.displayId = displayId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponseDto that = (UserResponseDto) o;
        return Objects.equals(displayId, that.displayId) && Objects.equals(username, that.username) && Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(displayId, username, firstName, lastName);
    }

    @Override
    public String toString() {
        return "RegistrationResponseDto{" +
                "displayId=" + displayId +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
