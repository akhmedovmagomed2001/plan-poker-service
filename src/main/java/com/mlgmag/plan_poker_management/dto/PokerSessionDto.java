package com.mlgmag.plan_poker_management.dto;

import java.util.Set;
import java.util.UUID;

public class PokerSessionDto {

    private UUID displayId;
    private UUID createdByDisplayId;
    private String title;
    private String description;
    private Set<UUID> participants;

    public PokerSessionDto() {
    }

    public PokerSessionDto(UUID displayId, UUID createdByDisplayId, String title, String description, Set<UUID> participants) {
        this.displayId = displayId;
        this.createdByDisplayId = createdByDisplayId;
        this.title = title;
        this.description = description;
        this.participants = participants;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public UUID getCreatedByDisplayId() {
        return createdByDisplayId;
    }

    public void setCreatedByDisplayId(UUID createdByDisplayId) {
        this.createdByDisplayId = createdByDisplayId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UUID> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<UUID> participants) {
        this.participants = participants;
    }
}
