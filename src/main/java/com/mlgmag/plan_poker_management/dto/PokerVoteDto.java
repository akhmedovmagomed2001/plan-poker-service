package com.mlgmag.plan_poker_management.dto;

import java.util.UUID;

public class PokerVoteDto {

    private UUID displayId;
    private UUID votedByDisplayId;
    private UUID votedStoryDisplayId;
    private Integer value;

    public PokerVoteDto() {
    }

    public PokerVoteDto(UUID displayId, UUID votedByDisplayId, UUID votedStoryDisplayId, Integer value) {
        this.displayId = displayId;
        this.votedByDisplayId = votedByDisplayId;
        this.votedStoryDisplayId = votedStoryDisplayId;
        this.value = value;
    }

    public UUID getDisplayId() {
        return displayId;
    }

    public void setDisplayId(UUID displayId) {
        this.displayId = displayId;
    }

    public UUID getVotedByDisplayId() {
        return votedByDisplayId;
    }

    public void setVotedByDisplayId(UUID votedByDisplayId) {
        this.votedByDisplayId = votedByDisplayId;
    }

    public UUID getVotedStoryDisplayId() {
        return votedStoryDisplayId;
    }

    public void setVotedStoryDisplayId(UUID votedStoryDisplayId) {
        this.votedStoryDisplayId = votedStoryDisplayId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
