package com.mlgmag.plan_poker_management.service.impl;

import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.repository.UserRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final String USERNAME = "joe_doe";
    private static final String PASSWORD = "password";
    private static final String ENCODED_PASSWORD = "encoded_password";
    private static final UUID DISPLAY_ID = UUID.randomUUID();
    private static final Long ID = 123L;

    private static User user;

    @Mock
    private UserRepository userRepository;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @Captor
    private ArgumentCaptor<User> userCaptor;

    @InjectMocks
    private UserServiceImpl testInstance;

    @BeforeAll
    static void setup() {
        user = new User();
        user.setPassword(PASSWORD);
    }

    @Test
    void shouldCreate() {
        when(passwordEncoder.encode(PASSWORD)).thenReturn(ENCODED_PASSWORD);
        when(userRepository.save(user)).thenReturn(user);

        User actual = testInstance.create(user);

        assertThat(actual).isEqualTo(user);

        verify(userRepository).save(userCaptor.capture());
        User userCaptorValue = userCaptor.getValue();

        assertThat(userCaptorValue.getPassword()).isEqualTo(ENCODED_PASSWORD);
        assertThat(userCaptorValue.getDisplayId()).isNotNull();
    }

    @Test
    void shouldGetAll() {
        when(userRepository.findAll()).thenReturn(List.of(user));

        List<User> actual = testInstance.getAll();

        assertThat(actual)
                .isNotNull()
                .hasSize(1)
                .contains(user);
    }

    @Test
    void shouldFindByDisplayId() {
        when(userRepository.findByDisplayId(DISPLAY_ID)).thenReturn(user);

        Optional<User> actual = testInstance.findByDisplayId(DISPLAY_ID);

        assertThat(actual).isPresent().contains(user);
    }

    @Test
    void shouldFindByDisplayIdOnNull() {
        when(userRepository.findByDisplayId(DISPLAY_ID)).thenReturn(null);

        Optional<User> actual = testInstance.findByDisplayId(DISPLAY_ID);

        assertThat(actual).isEmpty();
    }

    @Test
    void shouldFindByUsername() {
        when(userRepository.findByUsername(USERNAME)).thenReturn(user);

        Optional<User> actual = testInstance.findByUsername(USERNAME);

        assertThat(actual).isPresent().contains(user);
    }

    @Test
    void shouldFindByUsernameOnNull() {
        when(userRepository.findByUsername(USERNAME)).thenReturn(null);

        Optional<User> actual = testInstance.findByUsername(USERNAME);

        assertThat(actual).isEmpty();
    }

    @Test
    void shouldFindById() {
        when(userRepository.findById(ID)).thenReturn(Optional.of(user));

        Optional<User> actual = testInstance.findById(ID);

        assertThat(actual).isPresent().contains(user);
    }

    @Test
    void shouldFindByIdOnNull() {
        when(userRepository.findById(ID)).thenReturn(Optional.empty());

        Optional<User> actual = testInstance.findById(ID);

        assertThat(actual).isEmpty();
    }
}
