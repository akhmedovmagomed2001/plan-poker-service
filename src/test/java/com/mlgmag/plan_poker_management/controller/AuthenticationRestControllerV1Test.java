package com.mlgmag.plan_poker_management.controller;

import com.mlgmag.plan_poker_management.converter.Converter;
import com.mlgmag.plan_poker_management.dto.AuthenticationRequestDto;
import com.mlgmag.plan_poker_management.dto.UserRequestDto;
import com.mlgmag.plan_poker_management.dto.UserResponseDto;
import com.mlgmag.plan_poker_management.jwt.JwtTokenProvider;
import com.mlgmag.plan_poker_management.model.User;
import com.mlgmag.plan_poker_management.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthenticationRestControllerV1Test {

    private static final String USERNAME = "joe_doe";
    private static final String PASSWORD = "password";
    private static final String TOKEN = "token";
    private static final String USER_ALREADY_EXISTS_MESSAGE = "User with username 'joe_doe' already exists";
    private static final String USER_NOT_FOUND_MESSAGE = "User with username 'joe_doe' not found";

    private static User user;
    private static AuthenticationRequestDto authenticationRequestDto;
    private static UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken;
    private static UserRequestDto userRequestDto;

    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private JwtTokenProvider jwtTokenProvider;
    @Mock
    private UserService userService;
    @Mock
    private Converter<UserRequestDto, User> registrationRequestToUserConverter;
    @Mock
    private Converter<User, UserResponseDto> userToRegistrationResponseDtoConverter;

    @InjectMocks
    private AuthenticationRestControllerV1 testInstance;

    @BeforeAll
    static void setup() {
        user = new User();
        user.setUsername(USERNAME);

        authenticationRequestDto = new AuthenticationRequestDto();
        authenticationRequestDto.setUsername(USERNAME);
        authenticationRequestDto.setPassword(PASSWORD);

        usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(USERNAME, PASSWORD);

        userRequestDto = new UserRequestDto();
    }

    @Test
    void shouldLoginOnExistingUser() {
        when(userService.findByUsername(USERNAME)).thenReturn(Optional.of(user));
        when(jwtTokenProvider.createToken(USERNAME)).thenReturn(TOKEN);

        ResponseEntity<Map<String, String>> actual = testInstance.login(authenticationRequestDto);

        Map<String, String> actualBody = actual.getBody();
        assertThat(actualBody).isNotNull();

        String actualUsername = actualBody.get("username");
        assertThat(actualUsername).isEqualTo(USERNAME);

        String actualToken = actualBody.get("token");
        assertThat(actualToken).isEqualTo(TOKEN);

        verify(authenticationManager).authenticate(usernamePasswordAuthenticationToken);
    }

    @Test
    void shouldLoginOnNotExistingUser() {
        when(userService.findByUsername(USERNAME)).thenReturn(Optional.empty());

        UsernameNotFoundException actual = assertThrows(
                UsernameNotFoundException.class,
                () -> testInstance.login(authenticationRequestDto)
        );

        assertThat(actual.getMessage()).isEqualTo(USER_NOT_FOUND_MESSAGE);

        verify(authenticationManager).authenticate(usernamePasswordAuthenticationToken);
    }

    @Test
    void shouldRegisterOnNewUser() {
        var expected = new UserResponseDto();

        when(registrationRequestToUserConverter.convert(userRequestDto)).thenReturn(user);
        when(userService.findByUsername(USERNAME)).thenReturn(Optional.empty());
        when(userService.create(user)).thenReturn(user);
        when(userToRegistrationResponseDtoConverter.convert(user)).thenReturn(expected);

        UserResponseDto actual = testInstance.register(userRequestDto);

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void shouldRegisterOnExistingUser() {
        when(registrationRequestToUserConverter.convert(userRequestDto)).thenReturn(user);
        when(userService.findByUsername(USERNAME)).thenReturn(Optional.of(user));

        ResponseStatusException actual = assertThrows(
                ResponseStatusException.class,
                () -> testInstance.register(userRequestDto)
        );

        assertThat(actual.getReason()).isEqualTo(USER_ALREADY_EXISTS_MESSAGE);
        assertThat(actual.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }
}
