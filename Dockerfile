FROM gradle:jdk21-alpine AS build
RUN mkdir /project
COPY . /project
WORKDIR /project
RUN gradle clean build -x test


FROM amazoncorretto:21
RUN mkdir /app
COPY --from=build /project/build/libs/plan_poker_service.jar /app/plan_poker_service.jar
WORKDIR /app
ENTRYPOINT ["java", "-jar", "plan_poker_service.jar"]
